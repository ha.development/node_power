// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import SuiVue from 'semantic-ui-vue'
import VeeValidate from 'vee-validate'
import moment from 'moment'

import Axios from 'axios'

import 'semantic-ui-css/semantic.min.css'

Vue.prototype.$http = Axios.create({
  baseURL: 'http://127.0.0.1:3333'
})

Vue.filter('timeAgo', date => moment(date).fromNow())
Vue.filter('joined', date => moment(date).format('MMMM YYYY'))
Vue.filter('dob', date => moment(date).format('MMMM Do YYYY'))

Vue.use(VeeValidate)
Vue.use(SuiVue)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
