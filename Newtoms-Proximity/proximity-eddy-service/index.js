const express = require('express');
const app = express();
const targetBaseUrl = 'https://www.new.pro/';
const axios = require('axios')

function handleRedirect(req, res) {
  const targetUrl = targetBaseUrl + req.params.id;
  res.redirect(targetUrl);
}

app.get('/:id', handleRedirect);

app.get('*', function(req, res){
    res.status(404).render('404.html', {title: "Sorry, page not found"});
  });

const port = process.env.port || 3000;

app.listen(port);