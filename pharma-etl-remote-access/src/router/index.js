import Vue from 'vue'
import Router from 'vue-router'
import RemoteAccess from '@/components/RemoteAccess'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'RemoteAccess',
      component: RemoteAccess
    }
  ]
})
