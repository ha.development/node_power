import Vue from 'nativescript-vue';
// import VueRouter from 'vue-router';
import Master from './components/Master';

import './app.scss'
// Vue.use(VueRouter);

// Uncommment the following to see NativeScript-Vue output logs
Vue.config.silent = false;

new Vue({
  render: h => h('frame', [h(Master)])
}).$start();

