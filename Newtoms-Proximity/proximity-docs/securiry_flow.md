```mermaid
graph LR
    App-->SSO
    App-->API
    subgraph API - Request: BaaS
        API-->Resources
        Resources-->/create
        Resources-->/update
        Resources-->/delete
        Resources-->/read
        subgraph admin/user
            subgraph  client/user
                /create
                /update
                /read
            end
            /delete
        end
    end
    subgraph User Authentication
        SSO["SSO: Single Sing On"]-->IDM
        IDM["IDM: Identity Manager"]-->IdP
        IdP[IdP: Identity Provider]-.->Choice
        Choice{Or}-.->OpenID
        Choice{Or}-.->SAML
        OpenID-->CheckUser
        SAML-->CheckUser
        subgraph IdP Database
            CheckUser["Verify user form the identity provider database"]
        end
    end
```