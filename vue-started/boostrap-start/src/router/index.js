import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Galleries from '@/components/Galleries'
import Contact from '@/components/Contact'
import Clients from '@/components/Clients'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/galleries',
      name: 'Galleries',
      component: Galleries
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/clients',
      name: 'Clients',
      component: Clients
    }
  ]
})
