import request from '../../shared/lib/request'

function singIn(data) {
    return request({
      url:    `/login`,
      method: 'POST',
      data: data
    });
  }
  
  function singUp({subject, content}) {
    return request({
      url:    '/message/create',
      method: 'POST',
      data:   {
        subject,
        content
      }
    });
  }
  
  const access = {
    singIn, singUp //, update, delete, etc. ...
  }
  
  export default access;